# Laetus Theme
### Dark and vibrant theme with minimalist flat UI and colorful syntax.

[![](https://vsmarketplacebadge.apphb.com/version/10f.laetus.svg)](https://marketplace.visualstudio.com/items?itemName=10f.laetus)&nbsp;
[![](https://img.shields.io/visual-studio-marketplace/stars/10f.laetus.svg)](https://marketplace.visualstudio.com/items?itemName=10f.laetus)

<!-- [![Version](https://vsmarketplacebadge.apphb.com/version/s3gf4ult.monokai-vibrant.svg)](https://marketplace.visualstudio.com/items?itemName=s3gf4ult.monokai-vibrant)
[![Installs](https://vsmarketplacebadge.apphb.com/installs/s3gf4ult.monokai-vibrant.svg)](https://marketplace.visualstudio.com/items?itemName=s3gf4ult.monokai-vibrant)
[![Ratings](https://vsmarketplacebadge.apphb.com/rating/s3gf4ult.monokai-vibrant.svg)](https://marketplace.visualstudio.com/items?itemName=s3gf4ult.monokai-vibrant) -->

![Monokai Vibrant JavaScript Example](https://gitlab.com/10F/laetus/raw/master/images/screenshots/Screenshot_1.png)
![Monokai Vibrant JavaScript Example](https://gitlab.com/10F/laetus/raw/master/images/screenshots/Screenshot_2.png)

### For the vibrancy effect, I am using this [Vibrancy Extension](https://marketplace.visualstudio.com/items?itemName=eyhn.vscode-vibrancy)

### Enjoy!